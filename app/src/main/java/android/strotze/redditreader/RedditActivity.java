package android.strotze.redditreader;


import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;


public class RedditActivity extends SingleFragmentActitvity implements ActivityCallback{
    MediaPlayer mp;

    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        return new RedditListFragment();
    }

    @Override
    public void onPostSelected(Uri redditPostUri) {
        mp = MediaPlayer.create(getApplicationContext(), R.raw.soundclick);

        mp.start();
        if(findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
            intent.setData(redditPostUri);
            startActivity(intent);

        }
        else{
            Fragment detailFragment = RedditWebFragment.newfragment(redditPostUri.toString());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, detailFragment)
                    .commit();
        }
    }
}
